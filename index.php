<?php
    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    // Release 0
    echo "<h1>Release 0</h1>";
    echo "<h3>Kelas Animal</h3>";
    $sheep = new Animal("shaun");
    
    echo "Nama: " . $sheep->getName() . '<br>'; // "shaun"
    echo "Jumlah kaki: " . $sheep->getLegs() . '<br>'; // 2
    echo "Darah Dingin: " . $sheep->getColdBlooded() . '<br>'; // false

    
    // Release 1
    echo "<h1>Release 1</h1>";
    echo "<h3>Kelas Ape</h3>";
    $sungokong = new Ape("kera sakti");
    echo "Nama: " . $sungokong->getName() . '<br>';
    echo "Jumlah kaki: " . $sungokong->getLegs() . '<br>';
    echo "Darah Dingin: " . $sungokong->getColdBlooded() . '<br>';
    echo "Method yell(): ";
    $sungokong->yell(); // "Auooo"

    echo "<h3>Kelas Frog</h3>";
    $kodok = new Frog("buduk");
    echo "Nama: " . $kodok->getName() . '<br>';
    echo "Jumlah kaki: " . $kodok->getLegs() . '<br>';
    echo "Darah Dingin: " . $kodok->getColdBlooded() . '<br>';
    echo "Method jump(): ";
    $kodok->jump(); // "hop hop"
?>